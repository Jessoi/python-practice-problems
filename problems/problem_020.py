# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):

    calc50 = len(attendees_list) / len(members_list)

    if calc50 >= 0.5:
        print("Greater than 50%")
    else:
        print("Less than 50%")

attendees = ["John", "Jones", "Jeremy", "Peter", "Parker", "Jane", "George", "jorge"]
members = ["John", "Jones", "Jeremy", "Peter", "Parker", "Jane", "George", "jorge"]

has_quorum(attendees, members)