# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    sum = 0
    avg = 0
    for num in values:
        sum += num
    avg = sum / len(values)

    return (avg)

values = [1, 2, 3]

print(calculate_average(values))
