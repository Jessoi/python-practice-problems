# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    items = []
    if not is_sunny and is_workday:
        items.append("umbrella")
    if is_workday:
        items.append("laptop")
    if not is_workday:
        items.append("surfboard")

    return(items)



is_workday = False
is_sunny = False

print(gear_for_day(is_workday, is_sunny))
