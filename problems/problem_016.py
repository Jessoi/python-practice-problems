# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if x <= 10 and x >= 0:
        print("X is in bounds")
    else:
        print("X is out of bounds")

    if y <= 10 and y >= 0:
        print("Y is in bounds")
    else:
        print("Y is out of bounds")

x = 0
y = 5

is_inside_bounds(x, y)