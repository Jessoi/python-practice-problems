# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) >= 1:
        return max(values)
    else:
        return None

values = [0, 5, 4, 6, 3]


print(max_in_list(values))