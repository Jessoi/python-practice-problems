# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def split_list(numbers):
    first = []
    second = []
    if len(numbers) % 2 == 0:
        half = len(numbers)/2
    else:
        half = (len(numbers) + 1)/2

    i = 0
    while i < len(numbers):
        if (i + 1) <= half:
            first.append(numbers[i])
        else:
            second.append(numbers[i])
        i += 1

    return (first, second)

numbers = [1, 2, 3, 4, 5, 6, 7]
print(split_list(numbers))