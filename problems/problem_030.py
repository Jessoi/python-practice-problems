# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    largest = 0
    second = 0
    for num in values:
        if num >= largest:
            second = largest
            largest = num

    if second == 0:
        return None
    else:
        return second

values = [1, 2, 3, 4, 5]

print(find_second_largest(values))
