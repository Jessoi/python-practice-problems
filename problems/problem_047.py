# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lengthmin = 6
    lengthmax = 12

    digit = False
    upper = False
    lower = False
    special = False

    if len(password) >= lengthmax:
            return("Invalid Password")
    if len(password) <= lengthmin:
            return("Invalid Password")
    #add is upper and lower inside isalpha
    for char in password:
        if char.isalpha():
            if char.isupper():
                 upper = True
            else:
                 lower = True
        if digit == False and char.isdigit():
            digit = True
        if special == False:
             if char == "!" or char == "$" or char == "@":
                  special = True
    if special and digit and upper and lower:
         return ("Valid Password")
    else:
         return ("Invalid Password")

password = "Abc12345!"

print(check_password(password))
