# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average
import problem_024

def calculate_grade(values):
    avg = problem_024.calculate_average(values)
    if avg >= 90:
        print ("A")
    elif avg >= 80:
        print ("B")
    elif avg >= 70:
        print ("C")
    elif avg >= 70:
        print ("D")
    else:
        print ("F")

values = [90, 90, 90, 0, 0]
calculate_grade(values)