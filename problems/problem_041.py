# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.



import problem_025

def add_csv_lines(csv_lines):
    newlist = []
    #Converts each index into lists "splat"
    for line in csv_lines:
        print(line)
        splat = line.split(",")
        print(splat)
        sum = 0
        #for each list in splat, convert to int and add into a sum
        for part in splat:
            intpart = int(part)
            sum += intpart
        # add sum into new list
        newlist.append(sum)
    #returning the finished product
    return(newlist)

csv_lines = ["3", "1,9"]
print(add_csv_lines(csv_lines))
